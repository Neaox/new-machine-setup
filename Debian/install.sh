#!/bin/bash
set -e

#Update apt
sudo apt-get update
sudo apt-get -y autoremove
sudo apt-get -y dist-upgrade

#Install golang development tools
sudo apt-get -y install golang

#Install general development tools
sudo apt-get -y install htop iotop

#TODO: add non-free repository

#Install general purpose utilities
sudo apt-get -y install preload lshw tmux tree

#Install Chromium extensions
if [ -n "$DISPLAY" ]
then
    sudo apt-get -y install wireshark

    #Install general purpose applications
    sudo apt-get -y install keepass2 chromium pepperflashplugin-nonfree

    #Consider installing apps to keep the kids busy
    #sudo apt-get -y install tuxpaint tuxpaint-config

    #TODO: learn how to DRY this up in bash
    chromium --install-from-webstore=jnihajbhpnppcggbcgedagnkighmdlei --apps-gallery-install-auto-confirm-for-tests=accept
    chromium --install-from-webstore=dbepggeogbaibhgnhhndojpepiihcmeb --apps-gallery-install-auto-confirm-for-tests=accept
    chromium --install-from-webstore=cfhdojbkjhnklbpkdaibdccddilifddb --apps-gallery-install-auto-confirm-for-tests=accept
    chromium --install-from-webstore=febilkbfcbhebfnokafefeacimjdckgl --apps-gallery-install-auto-confirm-for-tests=accept
    chromium --install-from-webstore=gcbommkclmclpchllfjekcdonpmejbdp --apps-gallery-install-auto-confirm-for-tests=accept
else
    echo -e "\033[1;33mThere is no X server running - unable to install GUI apps.\033[0m"
fi

#Install VIM (this should be extracted into the VIM repository)
if [ -d "$HOME/.vim" ]; then
    echo "It looks like there is already a VIM installation."
else
    sudo apt-get -y install vim-nox python git
    pushd ~
    git clone https://bartj@bitbucket.org/bartj/vim.git .vim
    cd .vim
    git submodule init
    git submodule update
    cd ..
    ln .vim/vimrc .vimrc
    ln .vim/gvimrc .gvimrc
    popd
fi

#Detect e4rat
#This should be semi-automatable:
#1. Download and install the package
#2. Install two grub boot menus - one to profile, and one to launch single mode
#3. Configure the main grub boot menu
#4. Issue instructions
if dpkg -l | grep -q e4rat
then
    echo "It looks like e4rat is installed; assuming it has been set up correctly."
else
    echo -e "\033[1;33mIt looks like e4rat is not installed. I would recommend installing it for hard disk (not SSD) based systems.\033[0m"
fi


#Install zsh
#TODO: Replace with antigen as described here: https://github.com/arialdomartini/oh-my-git
if [ -d "$HOME/.oh-my-zsh" ]; then
    echo "It looks like oh-my-zsh is already installed."
else
    sudo apt-get -y install curl zsh git
        
    curl -L https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh
    chsh -s /bin/zsh
    sed -ri 's/plugins=\(git\)/plugins=(git debian golang vi-mode)/' ~/.zshrc
    sed -ri 's/ZSH_THEME="robbyrussell"/ZSH_THEME="robbyrussell"/' ~/.zshrc

    git clone https://github.com/olivierverdier/zsh-git-prompt.git ~/.zsh-git-prompt
    echo source ~/.zsh-git-prompt/zshrc.sh >> ~/.zshrc
    echo PROMPT="'"'%B%m%~%b$(git_super_status) %# '"'" >> ~/.zshrc

    #Launch into the newly configured zsh
    zsh
fi

echo "Installation successful."
