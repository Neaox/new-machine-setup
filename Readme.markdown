Below are several one-liners for various operating systems.

##Cygwin##

	wget https://bitbucket.org/bartj/new-machine-setup/raw/master/Cygwin/Install.sh -O - | bash

##Debian Development (with GUI)##

	wget https://bitbucket.org/bartj/new-machine-setup/raw/master/Debian/install.sh -O - | bash
	
##Windows Development##

```PowerShell
iex (New-Object Net.Webclient).DownloadString('https://bitbucket.org/bartj/new-machine-setup/raw/master/WindowsDevelopment/Install.ps1')
```