﻿#requires -runasadministrator
#requires -version 4

$ErrorActionPreference = 'Stop'

function ValidateVisualStudioIsAlreadyInstalled()
{
    $requiredVisualStudioVersion = '14.0'
    if(!(Test-Path HKLM:\SOFTWARE\Microsoft\VisualStudio\$requiredVisualStudioVersion)) {
        throw "Expected Visual Studio $requiredVisualStudioVersion to be installed."
    }
}

function KillGitProcessThatStopsUpdatesFromWorking()
{
    Get-Process -ea 0 ssh-agent | Stop-Process -Force
}

function SetExecutionPolicy()
{
    Set-ExecutionPolicy RemoteSigned
}

function EnsureChocolateyInstalled()
{
    if(!(gcm cinst -ErrorAction SilentlyContinue)) {
        iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
        $env:path += "$($env:SYSTEMDRIVE)\chocolatey\bin"
    }
}

function SetHighPerformancePowerProfile()
{
    powercfg -setactive 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c
    if(!$?) {
        throw "Failed to set power plan to 'High performance'."
    }
}

function InstallChromeExtensions()
{
    $alreadyInstalled = ls -ea 0 (join-path ([Environment]::GetFolderPath('LocalApplicationData')) 'Google\Chrome\User Data\Default\Extensions') | select -expand Name

    'dbepggeogbaibhgnhhndojpepiihcmeb', <# Vimium #>
    'febilkbfcbhebfnokafefeacimjdckgl', <# Markdown Preview Plus #>
    'mpbpobfflnpcgagjijhmgnchggcjblin', <# HTTP/2 and SPDY indicator #>
    'bfbameneiokkgbdmiekhjnmfkcnldhhm', <# Web Developer toolbar #>
    'fhbjgbiflinjbdggehcddcbncdddomop'  <# Postman - REST Client (Packaged App) #> | % {
        if($alreadyInstalled -notcontains $_) {
            & "C:\Program Files (x86)\Google\Chrome\Application\Chrome.exe" --install-chrome-app=$_
        }
    }
}

function Update-PathVariablesForCurrentProcessFromRegistry
{
    [OutputType([Nullable])]
    param()

    function StripTrailingSemicolons([string]$Value)
    {
        if($Value) { $Value.TrimEnd(';') } else { $Value }
    }

    function UpdatePath([string][parameter(mandatory)]$EnvironmentVariableName, [string[]]$seed)
    {
        $newValues = $seed
        $newValues += StripTrailingSemicolons([System.Environment]::GetEnvironmentVariable($EnvironmentVariableName,'Machine')) -split ';'
        $newValues += StripTrailingSemicolons([System.Environment]::GetEnvironmentVariable($EnvironmentVariableName,'User')) -split ';'
        $newValue = StripTrailingSemicolons($newValues -join ';')

        $envName = "env:$EnvironmentVariableName"
        $currentValue = StripTrailingSemicolons((Get-Item -Path $envName).Value)
        if($currentValue -ne $newValue) {
            Set-Item -Path $envName -Value $newValue
            Write-Host -ForegroundColor Green "Updating $EnvironmentVariableName because the system variable was different to the local process variable, probably due to application installation."
            Write-Host
        }
    }

    UpdatePath PSModulePath ("$home\Documents\WindowsPowerShell\Modules", "$env:ProgramFiles\WindowsPowerShell\Modules")
    UpdatePath PATH @()
}

function InstallFromChocolatey()
{
    $tools =
        '7zip',
        '7zip.commandline',
        'ag',
        'beyondcompare',
        'dotPeek',
        'fiddler4',
        'filezilla',
        'Firefox',
        'gimp',
        'git /GitAndUnixToolsOnPath /NoAutoCrlf',
        'google-chrome-x64',
        'graphviz.portable',
        'hxd',
        'ilspy',
        'keepass',
        'kitty.portable',
        'linqpad4',
        'logparser',
        'microsoft-message-analyzer',
        'miktex',
        'ncrunch-vs2015',
        'nmap',
        'nodejs',
        'obs',
        'pandoc',
        'perfview',
        'planexplorerssmsaddin',
        'python2-x86_32',    # for pandoc + pygments; 32 bit for Vim
        'poshgit',
        'resharper',
        'rsat',
        #'screentogif',        -- commented out until issue is resolved installing to Windows 10
        'superbenchmarker',
        'sysinternals',
        'treesizefree',
        'windbg',
        'wireshark'

    $chocPath = Join-Path $env:ChocolateyInstall 'choco.exe'
    $alreadyInstalled = & $chocpath list --local-only | ? { ($_ -split ' ').length -eq 2 } | % { ($_ -split ' ')[0] }
    $newTools = $tools | ? { $alreadyInstalled -notcontains ($_ -split ' ', 2)[0] }

    Write-Host "Updating all currently installed components..."
    Update-PathVariablesForCurrentProcessFromRegistry
    & $chocPath upgrade all -y
    Update-PathVariablesForCurrentProcessFromRegistry

    Write-Host "Installing all new components..."
    $newTools | % {
        $package, $packageParameters = $_ -split ' ', 2

        if($packageParameters) {
            & $chocPath install -y $package -params $packageParameters
        }
        else {
            & $chocPath install -y $package
        }

        Update-PathVariablesForCurrentProcessFromRegistry
    }
}

function InstallPackagesUsingWindowsPackageManagement()
{
    Install-Package -Source psgallery pscx
}

function UpdatePowerShellProfile()
{
    $profileComment = "# Installed by Bart's BitBucket script"

    if((cat $profile) -contains $profileComment) {
        return
    }

    $commands = @($profileComment, "if(gmo -list Hyper-V) { ipmo Hyper-V }", "ipmo pscx -NoClobber", "Invoke-BatchFile `"`${env:VS140COMNTOOLS}\VsDevCmd.bat`"", $profileComment) -join "`r`n"

    Add-Content -Path $profile -Value "`r`n`r`n$commands`r`n"
}

function UpdatePowerShellHelp()
{
    Update-Help -Force -ErrorAction Continue
}

function CheckForResharperSettings()
{
    $fileName = 'ResharperSettings.DotSettings'
    $jetBrainsDirectory = Join-Path (Resolve-Path ~\AppData\Roaming) JetBrains
    $filePath = Join-Path $jetBrainsDirectory $fileName

    if(!(Test-Path $jetBrainsDirectory)) {
        Write-Host "ReSharper does not appear to be installed. Skipping configuration."
        return
    }

    if(ls $jetBrainsDirectory\Shared\vAny\GlobalSettingsStorage.DotSettings -ea 0 | sls $fileName) {
        Write-Host "ReSharper settings appear to already be in place."
    }

    $wc = New-Object System.Net.WebClient
    $wc.DownloadFile("https://bitbucket.org/bartj/new-machine-setup/raw/HEAD/WindowsDevelopment/$fileName", $filePath)
    Write-Warning "You will need to manually install the ReSharper settings file at $filePath."
}

function DisplayOptionalResharperSettings()
{
    'Potential ReSharper manual configuration:',
    '($DTE.Commands | ? { $_.Name -eq "ReSharper.ReSharper_DuplicateText" }).Bindings = @()' | % { Write-Host -ForegroundColor Green $_ }
}

function ConfigureGit()
{
    git config --global core.autocrlf false
    git config --global core.longpaths true
    git config --global core.preloadindex true
    git config --global core.whitespace cr-at-eol
    git config --global mergetool.keepbackup false
    git config --global mergetool.prompt false
    git config --global difftool.prompt false
    git config --global rerere.enabled true
    git config --global receive.denynonfastforwards true
    git config --global core.logallrefupdates true

    git config --global difftool.bc3.path "c:/program files/beyond compare 4/BCompare.exe"
    git config --global mergetool.bc3.path "c:/program files/beyond compare 4/BCompare.exe"

    git config --global difftool.vs.cmd 'devenv //diff """$LOCAL""" """$REMOTE"""'
    git config --global difftool.vs.keepbackup false
    git config --global difftool.vs.trustexitcode false

    git config --global difftool.vsdiffmerge.cmd 'vsdiffmerge.exe """$LOCAL""" """$REMOTE""" //t'
    git config --global difftool.vsdiffmerge.keepbackup false
    git config --global difftool.vsdiffmerge.trustexitcode false
    git config --global difftool.vsdiffmerge.prompt true    # this is required because vsdiffmerge exits immediately instead of waiting for VS to quit
    git config --global mergetool.vsdiffmerge.cmd 'vsdiffmerge.exe """$REMOTE""" """$LOCAL""" """$BASE""" """$MERGED""" //m'
    git config --global mergetool.vsdiffmerge.keepbackup false
    git config --global mergetool.vsdiffmerge.trustexitcode false
    git config --global mergetool.vsdiffmerge.prompt true    # this is required because vsdiffmerge exits immediately instead of waiting for VS to quit

    git config --global diff.tool bc3
    git config --global merge.tool bc3
}

function InstallPowerShellTimerPrompt()
{
    iex (New-Object Net.Webclient).DownloadString('https://gist.github.com/nzbart/5481021/raw/3-Installer.ps1')
}

function ConfigureDocumentGeneration()
{
    python -m pip install --upgrade pip pygments
    
    $path = [Environment]::GetEnvironmentVariable('PATH', 'Machine')
    $scriptPath = 'C:\tools\python2-x86_32\Scripts'
    if($path -notlike ("*" + $scriptPath + "*")) {
        [Environment]::SetEnvironmentVariable('PATH', "$PATH;$scriptPath", 'Machine')
    }
    
    Update-PathVariablesForCurrentProcessFromRegistry
}

function InstallVsixPackages()
{
    function InstallVsixPackage($packageName, $package)
    {
        $extensionAlreadyInstalled = ls -r ~\AppData\Local\Microsoft\VisualStudio\14.0\Extensions extension.vsixmanifest | cat | sls $packageName
        if($extensionAlreadyInstalled) {
            Write-Host "$packageName VSIX extension is already installed."
            return
        }

        Write-Host "Installing VSIX extension $packageName..."
        $url = "https://visualstudiogallery.msdn.microsoft.com/$package"
        $vsixInstaller = 'C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\VSIXInstaller.exe'
        $wc = New-Object System.Net.WebClient

        $tempFile = [IO.Path]::GetTempFileName()
        try {
            $wc.DownloadFile($url, $tempFile)
            $proc = Start-Process -PassThru -NoNewWindow -Wait $vsixInstaller '/q', $tempFile
            if($proc.ExitCode -ne 0) {
                throw "Failed to install VSIX extension: $packageName. Exit code = $($proc.ExitCode)."
            }
        }
        finally {
            rm $tempFile
        }
        Write-Host "Successfully installed $packageName."
    }

    InstallVsixPackage 'Visual Studio Spell Checker' a23de100-31a1-405c-b4b7-d6be40c3dfff/file/104494/5/VSSpellCheckerPackage.vsix
    InstallVsixPackage 'Web Essentials 2015' ee6e6d8c-c837-41fb-886a-6b50ae2d06a2/file/146119/19/WebEssentials2015.vsix
    InstallVsixPackage 'Microsoft CodeLens Code Health Indicator' 54b2682c-47f4-48ee-9d6d-190402330c49/file/177950/1/Microsoft.VisualStudio.CodeSense.CodeHealth.vsix
    InstallVsixPackage 'Fix File Encoding' 540ac2d8-f881-4794-8b00-810d28257b70/file/89822/3/FixFileEncoding_12.vsix
    InstallVsixPackage 'VSColorOutput' f4d9c2b5-d6d7-4543-a7a5-2d7ebabc2496/file/63103/9/VSColorOutput.vsix
    #InstallVsixPackage 'NoGit' 146b404a-3c91-46ff-932a-fb0f8b826f94/file/136149/2/NoGit.vsix                                            -- commented out because no 2015 support
}

ValidateVisualStudioIsAlreadyInstalled
KillGitProcessThatStopsUpdatesFromWorking
SetExecutionPolicy
SetHighPerformancePowerProfile
EnsureChocolateyInstalled
InstallFromChocolatey
InstallPackagesUsingWindowsPackageManagement
UpdatePowerShellProfile
UpdatePowerShellHelp
ConfigureGit
InstallChromeExtensions
CheckForResharperSettings
DisplayOptionalResharperSettings
ConfigureDocumentGeneration
#InstallPowerShellTimerPrompt   -- commented out because it doesn't appear to work correctly on Windows 10
InstallVsixPackages
